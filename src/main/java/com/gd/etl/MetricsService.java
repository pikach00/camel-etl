package com.gd.etl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.stereotype.Component;

@Component
public class MetricsService {

	public static final Object NAME = "metricsService";

	private final CounterService counterService;

	@Autowired
	public MetricsService(CounterService counterService) {
		this.counterService = counterService;
	}

	public void invoke(String name) {
		this.counterService.increment("route." + name + ".invoked.count");
	}

}
