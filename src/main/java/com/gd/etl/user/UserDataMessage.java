package com.gd.etl.user;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.gd.etl.message.Message;

@XmlRootElement(name = "Message")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserDataMessage extends Message {

	@XmlElement(name = "MessageBody")
	private UserMessageBody MessageBody;
	
	

	public UserMessageBody getMessageBody() {
		return MessageBody;
	}



	public void setMessageBody(UserMessageBody messageBody) {
		MessageBody = messageBody;
	}



	@Override
	public String toString() {
		return "UserDataMessage [MessageBody=" + MessageBody + ", getMessageHeader()=" + getMessageHeader()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

}
