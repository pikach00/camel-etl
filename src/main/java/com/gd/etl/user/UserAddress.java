package com.gd.etl.user;

import javax.xml.bind.annotation.XmlAttribute;

public class UserAddress extends TimeStampEntity {

	private Long id;

	private String country;

	private String city;

	private String address;

	private String phoneNo;

	private Long userID;

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	@XmlAttribute(name = "ID")
	public Long getID() {
		return id;
	}

	public void setID(Long id) {
		this.id = id;
	}

	@XmlAttribute(name = "Country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@XmlAttribute(name = "City")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@XmlAttribute(name = "Address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@XmlAttribute(name = "PhoneNo")
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Override
	public String toString() {
		return "UserAddress [ID=" + id + ", Country=" + country + ", City=" + city + ", Address=" + address
				+ ", PhoneNo=" + phoneNo + ", UserID=" + userID + "]";
	}

}
