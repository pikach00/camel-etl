package com.gd.etl.user;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

public class UserMessageBody {

	private User User;

	@XmlElement(name = "User")
	public User getUser() {
		return User;
	}

	public void setUser(User user) {
		User = user;
	}

}
