package com.gd.etl;

import org.apache.camel.component.metrics.messagehistory.MetricsMessageHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MetricsController {

	@Autowired
	Config config;

	@RequestMapping("/camelmetrics")
	public String getMetrics() {
		MetricsMessageHistoryService service = config.getCamelContext().hasService(MetricsMessageHistoryService.class);
		String json = service.dumpStatisticsAsJson();
		return json;
	}

}
