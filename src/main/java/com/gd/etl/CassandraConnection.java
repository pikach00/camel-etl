package com.gd.etl;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.UserType;

@Component
public class CassandraConnection {

	@Autowired
	private Config config;

	private Cluster cluster = null;

	private Session session = null;

	public ResultSet execute(String query) {
		return session.execute(query);
	}

	public ResultSet execute(Statement stmt) {
		return session.execute(stmt);
	}

	@PostConstruct
	private void initConnection() {
		cluster = Cluster.builder().addContactPoint(config.getCassandraHost()).withPort(config.getCassandraPort())
				.build();
		session = cluster.connect(config.getCassandraKeyspace());
	}

	@PreDestroy
	private void releaseConnection() {
		if (cluster != null) {
			cluster.close();
		}
	}
	
	public UserType getUDT(String type){
		UserType result = cluster.getMetadata()
			     .getKeyspace(config.getCassandraKeyspace())
			     .getUserType(type);
		return result;
	}
	


}
