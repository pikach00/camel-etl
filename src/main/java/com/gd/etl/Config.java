package com.gd.etl;

import org.apache.camel.CamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

	@Autowired
	CamelContext camelContext;
	
	
	
	public CamelContext getCamelContext() {
		return camelContext;
	}

	private @Value("${kafka.host}") String kafkaHost;

	private @Value("${kafka.params}") String kafkaParams;

	private @Value("${cassandra.host}") String cassandraHost;

	private @Value("${cassandra.keyspace}") String cassandraKeyspace;

	private @Value("${cassandra.port}") int cassandraPort;
	
	private @Value("${kafka.topics.orders}") String ordersTopic;

	private @Value("${kafka.topics.userdata}") String userdataTopic;

	public String getKafkaHost() {
		return kafkaHost;
	}

	public void setKafkaHost(String kafkaHost) {
		this.kafkaHost = kafkaHost;
	}

	public String getKafkaParams() {
		return kafkaParams;
	}

	public void setKafkaParams(String kafkaParams) {
		this.kafkaParams = kafkaParams;
	}

	public String getCassandraHost() {
		return cassandraHost;
	}

	public void setCassandraHost(String cassandraHost) {
		this.cassandraHost = cassandraHost;
	}

	public int getCassandraPort() {
		return cassandraPort;
	}

	public void setCassandraPort(int cassandraPort) {
		this.cassandraPort = cassandraPort;
	}

	public String getOrdersTopic() {
		return ordersTopic;
	}

	public void setOrdersTopic(String ordersTopic) {
		this.ordersTopic = ordersTopic;
	}

	public String getUserdataTopic() {
		return userdataTopic;
	}

	public void setUserdataTopic(String userdataTopic) {
		this.userdataTopic = userdataTopic;
	}

	public String getCassandraKeyspace() {
		return cassandraKeyspace;
	}

	public void setCassandraKeyspace(String cassandraKeyspace) {
		this.cassandraKeyspace = cassandraKeyspace;
	}

	

}
