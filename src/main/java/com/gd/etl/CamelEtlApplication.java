package com.gd.etl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelEtlApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelEtlApplication.class, args);
	}
	
}
