package com.gd.etl.router;

import static org.apache.camel.builder.ExpressionBuilder.beanExpression;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gd.etl.MetricsService;
import com.gd.etl.dao.AddressDAO;
import com.gd.etl.dao.UserDAO;
import com.gd.etl.router.transform.UserMessageAddressAndTimestampEnreacher;
import com.gd.etl.router.transform.UserMessageAddressSplitter;
import com.gd.etl.router.transform.UserMessageToUserTransformer;
import com.gd.etl.user.UserDataMessage;

@Component 
public class UserDataRoute extends RouteBuilder {
	
	private static final String SPRING_METRICS_ENDPOINT = "bean:%s?method=invoke('%s')";

	private static final String METRICS_ENDPOINT = "metrics:counter:route.%s.invoked.count";

	private static final String CREATE_USER_ADDDRESS_ROUTE = "CREATE_USER_ADDDRESS";

	private static final String CREATE_USER_ROUTE = "CREATE_USER";

	private static final String PROCESS_USER_MESSAGE_ROUTE = "PROCESS_USER_MESSAGE";

	@Autowired
	UserRouteKafkaEndpointBuilder userRouteKafkaEndpointBuilder;

	@Override
	public void configure() {
		

		fromF("direct:%s", PROCESS_USER_MESSAGE_ROUTE)
			.convertBodyTo(UserDataMessage.class)
			.transform(beanExpression(UserMessageAddressAndTimestampEnreacher.NAME))
			.multicast()
			.to("direct:"+CREATE_USER_ROUTE, "direct:"+CREATE_USER_ADDDRESS_ROUTE);

		
		fromF(userRouteKafkaEndpointBuilder.build())
				.to("log:out")
				.choice()
				// check if it is message
				.when(simple("${body.toUpperCase()} contains '<MESSAGE>'"))
					.toF("direct:%s", PROCESS_USER_MESSAGE_ROUTE)
					.to("log:out")
				.endChoice()
					.otherwise()
					.to("log:err")
				.endChoice();

		
		fromF("direct:%s", CREATE_USER_ROUTE).routeId(CREATE_USER_ROUTE)
				.toF(METRICS_ENDPOINT, CREATE_USER_ROUTE)
				//just to check how spring counter implementation works
				.toF(SPRING_METRICS_ENDPOINT, MetricsService.NAME, CREATE_USER_ROUTE)
				.transform(beanExpression(UserMessageToUserTransformer.NAME))
				.to("log:out")
				.toF("bean:%s?method=create", UserDAO.NAME);

		fromF("direct:%s", CREATE_USER_ADDDRESS_ROUTE)
				.toF(METRICS_ENDPOINT, CREATE_USER_ADDDRESS_ROUTE)
				//just to check how spring counter implementation works
				.toF(SPRING_METRICS_ENDPOINT, MetricsService.NAME, CREATE_USER_ADDDRESS_ROUTE)
				.split(beanExpression(UserMessageAddressSplitter.NAME))
				.streaming()
				.toF("bean:%s?method=create", AddressDAO.NAME);
				
				
		
	}
	

}
