package com.gd.etl.router;

import static org.apache.camel.builder.ExpressionBuilder.beanExpression;

import org.apache.camel.ValidationException;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gd.etl.MetricsService;
import com.gd.etl.dao.OrderDAO;
import com.gd.etl.order.OrderMessage;
import com.gd.etl.router.transform.OrderMessageEnreacher;
import com.gd.etl.router.transform.OrderMessageToOrderTransformer;

@Component 
public class OrderRoute extends RouteBuilder {
	
	private static final String ORDER_MESSAGE_VALIDATION_ROUTE = "ORDER_MESSAGE_VALIDATION";

	private static final String SPRING_METRICS_ENDPOINT = "bean:%s?method=invoke('%s')";

	private static final String METRICS_ENDPOINT = "metrics:counter:route.%s.invoked.count";

	private static final String CREATE_ORDER_ROUTE = "CREATE_ORDER";

	private static final String CHANGE_ORDER_STATUS_ROUTE = "CHANGE_ORDER_STATUS";

	private static final String CANCEL_ORDER_ROUTE = "CANCEL_ORDER";

	@Autowired
	OrderRouteKafkaEndpointBuilder orderRouteKafkaEndpointBuilder;
	

	@Override
	public void configure() {
		fromF("direct:%s", ORDER_MESSAGE_VALIDATION_ROUTE)
		.choice()
		.when().xpath("//Message/MessageHeader/Action[.='CANCEL_ORDER']")
			.to("validator:xsd/order_cancel_message.xsd")
		.endChoice()
			.otherwise()
			.to("validator:xsd/order_create_update_message.xsd")
		.endChoice();

		
		
		from(orderRouteKafkaEndpointBuilder.build())
				.to("log:out")
				.choice()
				// check if it is message
				.when(simple("${body.toUpperCase()} contains '<MESSAGE>'"))
					.doTry()
						.toF("direct:%s", ORDER_MESSAGE_VALIDATION_ROUTE)
						.convertBodyTo(OrderMessage.class)
						.transform(beanExpression(OrderMessageEnreacher.NAME))
						.to("log:out")
						.recipientList(simple("direct:${body.getAction}"))
					.end()	
					.doCatch(ValidationException.class)
						.setBody(simple("${exception.message} on input: ${body}"))
						.to("log:err")
					.end()	
				.endChoice()
					.otherwise()
					.to("log:err")
				.endChoice();
				
				

		
		fromF("direct:%s", CREATE_ORDER_ROUTE).routeId(CREATE_ORDER_ROUTE)
				.toF(METRICS_ENDPOINT, CREATE_ORDER_ROUTE)
				//just to check how spring counter implementation works
				.toF(SPRING_METRICS_ENDPOINT, MetricsService.NAME, CREATE_ORDER_ROUTE)
				.transform(beanExpression(OrderMessageToOrderTransformer.NAME))
				.to("log:out")
				.toF("bean:%s?method=createOrUpdate", OrderDAO.NAME);

		fromF("direct:%s", CHANGE_ORDER_STATUS_ROUTE)
				.toF(METRICS_ENDPOINT, CHANGE_ORDER_STATUS_ROUTE)
				//just to check how spring counter implementation works
				.toF(SPRING_METRICS_ENDPOINT, MetricsService.NAME, CHANGE_ORDER_STATUS_ROUTE)
				.transform(beanExpression(OrderMessageToOrderTransformer.NAME))
				.to("log:out")
				.toF("bean:%s?method=createOrUpdate", OrderDAO.NAME);
				
		fromF("direct:%s", CANCEL_ORDER_ROUTE)
				.toF(METRICS_ENDPOINT, CANCEL_ORDER_ROUTE)
				//just to check how spring counter implementation works
				.toF(SPRING_METRICS_ENDPOINT, MetricsService.NAME, CANCEL_ORDER_ROUTE)
				.transform(beanExpression(OrderMessageToOrderTransformer.NAME))
				.to("log:out")
				.toF("bean:%s?method=cancel", OrderDAO.NAME);
				
		
	}


}
