package com.gd.etl.router;

public interface OrderRouteKafkaEndpointBuilder {

	public String build();

}
