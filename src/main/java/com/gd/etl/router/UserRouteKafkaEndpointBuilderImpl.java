package com.gd.etl.router;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gd.etl.Config;

@Component
public class UserRouteKafkaEndpointBuilderImpl implements UserRouteKafkaEndpointBuilder {

	@Autowired
	Config config;

	public String build() {
		return String.format("kafka:%s?topic=%s&%s", config.getKafkaHost(), config.getUserdataTopic(),  config.getKafkaParams());
	}

}
