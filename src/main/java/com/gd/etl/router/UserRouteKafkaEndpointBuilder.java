package com.gd.etl.router;

public interface UserRouteKafkaEndpointBuilder {

	public String build();

}
