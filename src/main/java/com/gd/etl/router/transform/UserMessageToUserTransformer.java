package com.gd.etl.router.transform;

import org.springframework.stereotype.Component;

import com.gd.etl.user.User;
import com.gd.etl.user.UserDataMessage;

@Component
public class UserMessageToUserTransformer {

	public static final String NAME = "userMessageToUserTransformer";

	public User transfor(UserDataMessage userData) {
		return userData.getMessageBody().getUser();
	}
}
