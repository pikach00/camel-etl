package com.gd.etl.router.transform;

import java.util.List;

import org.springframework.stereotype.Component;

import com.gd.etl.user.UserAddress;
import com.gd.etl.user.UserDataMessage;

@Component
public class UserMessageAddressSplitter {

	public static final String NAME = "userMessageAddressSplitter";

	public List<UserAddress> split(UserDataMessage userData) {
		return userData.getMessageBody().getUser().getAddresses();
	}

}
