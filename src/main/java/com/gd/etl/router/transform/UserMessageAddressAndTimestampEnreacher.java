package com.gd.etl.router.transform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gd.etl.user.User;
import com.gd.etl.user.UserDataMessage;

@Component
public class UserMessageAddressAndTimestampEnreacher {

	public static final String NAME = "userMessageAddressAndTimestampEnreacher";

	@Autowired
	DateParser dateParser;

	public UserDataMessage enreach(UserDataMessage userData) {
		long timestamp = dateParser.strDateToMillis(userData.getMessageHeader().getCreateDateTime());

		User user = userData.getMessageBody().getUser();
		userData.getMessageBody().getUser().getAddresses().forEach(addr -> {
			addr.setUserID(user.getID());
			addr.setTimestamp(timestamp);
		});
		user.setTimestamp(timestamp);
		return userData;
	}
}
