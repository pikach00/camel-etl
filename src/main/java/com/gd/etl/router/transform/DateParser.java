package com.gd.etl.router.transform;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class DateParser {

	private DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	public Long strDateToMillis(String dateStr) {
		LocalDateTime date = LocalDateTime.parse(dateStr, sdf);
		return date.toInstant(ZoneOffset.UTC).toEpochMilli();
	}
	
	public Date strToDate(String dateStr) {
		return new Date(strDateToMillis(dateStr));
	}


}
