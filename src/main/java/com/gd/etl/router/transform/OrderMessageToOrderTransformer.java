package com.gd.etl.router.transform;

import org.springframework.stereotype.Component;

import com.gd.etl.order.Order;
import com.gd.etl.order.OrderMessage;

@Component
public class OrderMessageToOrderTransformer {

	public static final String NAME = "orderMessageToOrderTransformer";

	public Order transfor(OrderMessage message) {
		return message.getMessageBody().getOrder();
	}
}
