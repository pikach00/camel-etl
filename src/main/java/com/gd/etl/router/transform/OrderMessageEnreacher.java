package com.gd.etl.router.transform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gd.etl.order.Order;
import com.gd.etl.order.OrderMessage;
import com.gd.etl.user.User;

@Component
public class OrderMessageEnreacher {

	public static final String NAME = "orderMessageEnreacher";

	@Autowired
	DateParser dateParser;

	public OrderMessage enreach(OrderMessage orderMessage) {
		long timestamp = dateParser.strDateToMillis(orderMessage.getMessageHeader().getCreateDateTime());

		Order order = orderMessage.getMessageBody().getOrder();
		User user = orderMessage.getMessageBody().getOrder().getUser();
		// orderMessage.getMessageBody().getOrder().getDeliveryAddress().getAddresses().forEach(addr
		// -> {
		// addr.setUserID(user.getID());
		// addr.setTimestamp(timestamp);
		// });
		order.setTimestamp(timestamp);
		return orderMessage;
	}
}
