package com.gd.etl.order;

import javax.xml.bind.annotation.XmlAttribute;

public class DeliveryAddress {

	private Long id;

	private String country;

	private String city;

	private String address;

	private String phoneNo;

	@XmlAttribute(name = "ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlAttribute(name = "Country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@XmlAttribute(name = "City")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@XmlAttribute(name = "Address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@XmlAttribute(name = "PhoneNo")
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Override
	public String toString() {
		return "DeliveryAddress [id=" + id + ", country=" + country + ", city=" + city + ", address=" + address
				+ ", phoneNo=" + phoneNo + "]";
	}

}
