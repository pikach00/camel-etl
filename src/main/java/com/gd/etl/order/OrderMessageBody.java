package com.gd.etl.order;

import javax.xml.bind.annotation.XmlElement;

public class OrderMessageBody {

	private Order order;

	@XmlElement(name = "Order")
	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "OrderMessageBody [order=" + order + "]";
	}

}
