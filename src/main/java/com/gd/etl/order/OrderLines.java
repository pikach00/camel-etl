package com.gd.etl.order;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "OrderLines")
public class OrderLines {

	private List<OrderLine> OrderLines;

	@XmlElement(name = "OrderLine")
	public List<OrderLine> getOrderLines() {
		return OrderLines;
	}

	public void setOrderLines(List<OrderLine> orderLines) {
		OrderLines = orderLines;
	}

	@Override
	public String toString() {
		return "OrderLines [OrderLines=" + OrderLines + "]";
	}

}
