package com.gd.etl.order;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.gd.etl.message.Message;

@XmlRootElement(name = "Message")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderMessage extends Message {

	public String getAction() {
		return getMessageHeader().getAction();
	}

	@XmlElement(name = "MessageBody")
	private OrderMessageBody MessageBody;

	public OrderMessageBody getMessageBody() {
		return MessageBody;
	}

	public void setMessageBody(OrderMessageBody messageBody) {
		MessageBody = messageBody;
	}

	@Override
	public String toString() {
		return "OrderMessage [MessageBody=" + MessageBody + ", getMessageHeader()=" + getMessageHeader() + "]";
	}

}
