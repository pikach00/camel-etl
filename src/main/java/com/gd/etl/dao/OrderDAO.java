package com.gd.etl.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datastax.driver.core.Statement;
import com.datastax.driver.core.UDTValue;
import com.datastax.driver.core.UserType;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.gd.etl.order.DeliveryAddress;
import com.gd.etl.order.Item;
import com.gd.etl.order.Order;
import com.gd.etl.order.OrderLine;
import com.gd.etl.router.transform.DateParser;
import com.gd.etl.user.User;

@Service(OrderDAO.NAME)
public class OrderDAO extends CassandraConnectionDAO {
	public static final String NAME = "orderDAO";
	
	@Autowired
	DateParser dateParser;

	public void createOrUpdate(Order order) {
		UDTValue userValue = populateOrderUser(order);
		UDTValue deliveryAddressValue = populateOrderAddress(order);
		List<UDTValue> lines = populateOrderLines(order);

		
		Statement updateOrder = QueryBuilder.update("ORDERS")
				.with(QueryBuilder.set("status", order.getStatus()))
		        .and(QueryBuilder.set("createdate", dateParser.strDateToMillis(order.getCreateDate())))
		        .and(QueryBuilder.set("user", userValue))
		        .and(QueryBuilder.set("address", deliveryAddressValue))
		        .and(QueryBuilder.set("orderlines", lines))
		        .where(QueryBuilder.eq("id", order.getId()));

		updateOrder.setDefaultTimestamp(order.getTimestamp());
		connection.execute(updateOrder);
	}

	private UDTValue populateOrderAddress(Order order) {
		DeliveryAddress address = order.getDeliveryAddress();

		UserType orderUserAddressType = connection.getUDT("order_delivery_address");
		UDTValue deliveryAddressValue = orderUserAddressType.newValue()
			    .setInt("id", address.getId().intValue())
				.setString("country", address.getCountry())
				.setString("city", address.getCity())
				.setString("address", address.getAddress())
				.setString("phoneNo", address.getPhoneNo());
		return deliveryAddressValue;
	}
	
	
	private UDTValue populateOrderItem(Item item){
		if(item == null){
			return null;
		}
		UserType orderItemType = connection.getUDT("order_item");

		UDTValue value = orderItemType.newValue()
			    .setInt("id", item.getId().intValue())
			    .setString("name", item.getName())
		        .setFloat("price", item.getPrice().floatValue());
		return value;
	}
	
	private UDTValue populateOrderLine(OrderLine orderLine, UDTValue orderItem){
		UserType orderLineType = connection.getUDT("order_line");

		UDTValue value = orderLineType.newValue()
			    .setInt("id", orderLine.getId().intValue())
			    .setInt("quantity", orderLine.getId().intValue())
			    .setString("status",orderLine.getStatus());
		if(orderItem != null){
			value.setUDTValue("item", orderItem);
		}
		return value;
	}

	private List<UDTValue> populateOrderLines(Order order) {
		 List<OrderLine> orderLines = order.getOrderLines().getOrderLines();
		 
		 List<UDTValue> result = new ArrayList<>();
		 for (OrderLine orderLine : orderLines) {
			 result.add(populateOrderLine(orderLine, populateOrderItem(orderLine.getItem())));
		}
		return result;
	}


	private UDTValue populateOrderUser(Order order) {
		User user = order.getUser();

		UserType orderUserType = connection.getUDT("order_user");
		UDTValue userValue = orderUserType.newValue()
			    .setInt("id", user.getID().intValue())
			    .setString("email", user.getEmail())
		        .setString("firstname", user.getFirstName())
		        .setString("lastname", user.getLastName());
		return userValue;
	}
	
	
	public void cancel(Order order) {
		 List<UDTValue> lines = populateOrderLines(order);
		
		Statement updateOrder = QueryBuilder.update("ORDERS")
				.with(QueryBuilder.set("status", order.getStatus()))
		        .and(QueryBuilder.set("orderlines", lines))
		        .where(QueryBuilder.eq("id", order.getId()));

		updateOrder.setDefaultTimestamp(order.getTimestamp());
		connection.execute(updateOrder);
	}


}
