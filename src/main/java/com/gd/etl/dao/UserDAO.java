package com.gd.etl.dao;

import org.springframework.stereotype.Service;

import com.datastax.driver.core.Statement;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.gd.etl.user.User;

@Service(UserDAO.NAME)
public class UserDAO extends CassandraConnectionDAO {

	public static final String NAME = "userDAO";

	public void create(User user) {
		Statement insertUser = QueryBuilder.update("USER")
				.with(QueryBuilder.set("email", user.getEmail()))
		        .and(QueryBuilder.set("firstname", user.getFirstName()))
		        .and(QueryBuilder.set("lastname", user.getLastName()))
		        .where(QueryBuilder.eq("id", user.getID()));

		insertUser.setDefaultTimestamp(user.getTimestamp());
		connection.execute(insertUser);
	}

}
