package com.gd.etl.dao;

import org.springframework.stereotype.Component;

import com.datastax.driver.core.querybuilder.Insert;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.gd.etl.user.UserAddress;

@Component
public class AddressDAO extends CassandraConnectionDAO {

	public static final String NAME = "addressDAO";

	public void create(UserAddress address) {
		Insert insertAddress = QueryBuilder.insertInto("ADDRESS")
				.value("id", address.getID())
				.value("userId", address.getUserID())
				.value("country", address.getCountry())
				.value("city", address.getCity())
				.value("address", address.getAddress())
				.value("phoneNo", address.getPhoneNo())
				.ifNotExists();
		connection.execute(insertAddress);
	}
}
