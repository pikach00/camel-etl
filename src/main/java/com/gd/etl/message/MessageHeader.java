package com.gd.etl.message;

import javax.xml.bind.annotation.XmlElement;

public class MessageHeader {

	@XmlElement
	private String CreateDateTime;

	@XmlElement
	private From From;

	@XmlElement
	private String Action;

	public String getAction() {
		return Action;
	}

	public void setAction(String action) {
		Action = action;
	}

	public String getCreateDateTime() {
		return CreateDateTime;
	}

	public void setCreateDateTime(String createDateTime) {
		CreateDateTime = createDateTime;
	}

	public From getFrom() {
		return From;
	}

	public void setFrom(From from) {
		From = from;
	}

	@Override
	public String toString() {
		return "MessageHeader [CreateDateTime=" + CreateDateTime + ", From=" + From + ", Action=" + Action + "]";
	}
	
	

}
