package com.gd.etl.message;

import javax.xml.bind.annotation.XmlAttribute;

public class From {

	private String app;

	private String nodeID;

	@XmlAttribute
	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	@XmlAttribute
	public String getNodeID() {
		return nodeID;
	}

	public void setNodeID(String nodeID) {
		this.nodeID = nodeID;
	}

	@Override
	public String toString() {
		return "From [app=" + app + ", nodeID=" + nodeID + "]";
	}
	
	

}
