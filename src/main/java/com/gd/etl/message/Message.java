package com.gd.etl.message;

import javax.xml.bind.annotation.XmlElement;

public class Message {

	@XmlElement(name = "MessageHeader")
	private MessageHeader MessageHeader;

	public MessageHeader getMessageHeader() {
		return MessageHeader;
	}

	public void setMessageHeader(MessageHeader messageHeader) {
		MessageHeader = messageHeader;
	}

	@Override
	public String toString() {
		return "Message [MessageHeader=" + MessageHeader + "]";
	}

	
}
