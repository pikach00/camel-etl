create table shopping.USER(ID int, FirstName text, LastName text, Email text, primary key (ID));

create table shopping.ADDRESS(ID int, UserID int, Country text, City text, Address text,PhoneNo text, primary key (ID, UserID));

create type shopping.order_user(ID int, FirstName text, LastName text, Email text);

create type shopping.order_delivery_address(ID int, Country text, City text, Address text,PhoneNo text);

create type shopping.order_delivery_address(ID int, Country text, City text, Address text,PhoneNo text);

create type shopping.order_item(id int, name text, price float);

create type shopping.order_line(id int, quantity int, Status text, item frozen <order_item>);

create table shopping.orders(id int, status text, createdate timestamp, user frozen <order_user>, address frozen <order_delivery_address>, orderlines set<frozen <order_line>>, primary key (id));



