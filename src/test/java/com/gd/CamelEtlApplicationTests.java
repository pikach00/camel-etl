package com.gd;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.gd.etl.CamelEtlApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { CamelEtlApplication.class })
public class CamelEtlApplicationTests {

	@Test
	public void contextLoads() {
	}

}
