package com.gd;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import com.gd.etl.router.UserRouteKafkaEndpointBuilder;

@Primary
@Profile("localtest")
public class UserRouteKafkaEndpointBuilderTest implements UserRouteKafkaEndpointBuilder {

	@Override
	public String build() {
		return "direct:startKafka";
	}

}
