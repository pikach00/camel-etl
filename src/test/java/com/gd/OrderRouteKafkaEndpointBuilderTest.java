package com.gd;

import com.gd.etl.router.OrderRouteKafkaEndpointBuilder;

public class OrderRouteKafkaEndpointBuilderTest implements OrderRouteKafkaEndpointBuilder {

	public static final String ENDPOINT = "direct:startKafkaOrderRoute";

	@Override
	public String build() {
		return ENDPOINT;
	}

}
