package com.gd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.gd.etl.router.OrderRouteKafkaEndpointBuilder;

@Configuration
public class OrderRouteKafkaTestEndpoint {

	@Primary
	@Bean("orderRouteKafkaEndpointBuilder")  
	public OrderRouteKafkaEndpointBuilder orderRouteKafkaEndpointBuilder() {
		return new OrderRouteKafkaEndpointBuilderTest();
	}

}
