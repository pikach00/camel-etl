package com.gd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.gd.etl.router.UserRouteKafkaEndpointBuilder;

@Configuration
public class UserRouterKafkaTestEndpoint {

	@Primary
	@Bean("userRouteKafkaEndpointBuilder")  
	public UserRouteKafkaEndpointBuilder userRouteKafkaEndpointBuilder() {
		return new UserRouteKafkaEndpointBuilderTest();
	}

}
