package com.gd;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import com.gd.etl.dao.UserDAO;
import com.gd.etl.user.User;

@RunWith(CamelSpringRunner.class)
@SpringBootTest(classes = { OrderRouteKafkaTestEndpoint.class, UserRouterKafkaTestEndpoint.class})
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.gd.etl")
public class OrderRouteTest extends Assert {

	@Autowired
	CamelContext camelContext;

	@Produce(uri = OrderRouteKafkaEndpointBuilderTest.ENDPOINT)
	protected ProducerTemplate template;

	@EndpointInject(uri = "mock:direct:routeend")
	protected MockEndpoint createUserEndpoint;

	@Test
	public void testIntegrationRoute() throws Exception {
		System.err.println(camelContext.getEndpointMap());
		System.err.println(camelContext.getEndpointMap());
		System.err.println(camelContext.getEndpointMap());
		System.err.println(camelContext.getEndpointMap());

		// advice the first route using the inlined route builder
		camelContext.getRouteDefinition("CREATE_USER").adviceWith(camelContext, new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				// intercept sending to mock:foo and do something else
				interceptSendToEndpoint("bean:" + UserDAO.NAME + "?method=create").skipSendToOriginalEndpoint()
						.to("mock:direct:routeend");
			}
		});
		String messageBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Message><MessageHeader><CreateDateTime>2016-09-15T10:10:05.617Z</CreateDateTime><From app=\"AuthService\" nodeID=\"10.3.17.142\"/><Action>CREATE_USER</Action></MessageHeader><MessageBody><User ID=\"190\" FirstName=\"Alex\" LastName=\"Brown\" Email=\"abrown@gmal.com\"><Addresses><Address ID=\"1\" Country=\"US\" City=\"Chicago\" Address=\"Washington st. 12N123\" PhoneNo=\"123-456-0\"/><Address ID=\"2\" Country=\"UK\" City=\"London\" Address=\"Brown st. 4F/123\" PhoneNo=\"376-03-123\"/></Addresses></User></MessageBody></Message>";

		// MockEndpoint mock2 =
		// camelContext.getModelJAXBContextFactory("mock:bean:" + UserDAO.NAME +
		// "?method=create", MockEndpoint.class);
		// moc
		//
		// createUserEndpoint.setResultWaitTime(10000);
		createUserEndpoint.expectedMessageCount(1);
		//
		User user = new User();
		user.setID(190l);
		createUserEndpoint.expectedBodiesReceived(user);

		template.sendBody(messageBody);

		createUserEndpoint.assertIsSatisfied();

	}

}
